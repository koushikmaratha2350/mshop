import React from 'react';
import {SafeAreaView, ScrollView, StatusBar, StyleSheet} from 'react-native';
import Root from './src/Root';
import {Provider} from 'react-redux';
import {Store} from './src/redux/Store';

const App = () => (
  <Provider store={Store}>
    <Root />
  </Provider>
);

const styles = StyleSheet.create({});

export default App;
