import * as ActionTypes from './ActionTypes';
import * as AuthServices from '../services/Auth';
import * as Storage from '../../utils/Storage';

export const Login = (userName, password) => {
  return (dispatch, getState) => {
    dispatch({type: ActionTypes.POST_SIGNIN_PENDING});
    AuthServices.Login({userName, password})
      .then(response => {
        try {
          Storage.saveUserData(response.result);
          dispatch({
            type: ActionTypes.POST_SIGNIN_SUCCESS,
            payload: response.result,
          });
        } catch (e) {
          console.log(e);
        }
      })
      .catch(() => {
        dispatch({
          type: ActionTypes.POST_SIGNIN_FAILURE,
          message: 'Please enter valid credentials!',
        });
      });
  };
};

export const Register = (email, name, mobile, password) => {
  return (dispatch, getState) => {
    dispatch({type: ActionTypes.POST_SIGNUP_PENDING});
    AuthServices.Register({
      userName: '',
      email,
      name,
      userType: 'doctor',
      mobile,
      password,
    })
      .then(response => {
        Storage.saveUserData(response.result);
        try {
          dispatch({
            type: ActionTypes.POST_SIGNUP_SUCCESS,
            payload: response.result,
          });
        } catch (e) {
          console.log(e);
        }
      })
      .catch(err => {
        console.log('action failed', err);
        dispatch({
          type: ActionTypes.POST_SIGNUP_FAILURE,
          message: 'Please enter valid credentials!',
        });
      });
  };
};
