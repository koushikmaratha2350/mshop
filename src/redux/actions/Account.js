import * as ActionTypes from './ActionTypes';
import * as AccountServices from '../services/Account';
import AsyncStorage from '@react-native-async-storage/async-storage';


export const GetDashboardData = () => {
    return ((dispatch, getState) => {
        dispatch({ type: ActionTypes.GET_DASHBOARD_PENDING });
        AccountServices.getDashboard()
            .then((response) => {
                try {
                    dispatch({ type: ActionTypes.GET_DASHBOARD_SUCCESS, payload: response.result });
                } catch (e) {
                    console.log(e);
                }
            })
            .catch(() => {
                dispatch({ type: ActionTypes.GET_DASHBOARD_FAILURE, message: 'Error Processing Data' });
            })
    })
};