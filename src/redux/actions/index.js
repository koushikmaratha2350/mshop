import * as  AuthActions from './Auth';
import * as  AccountActions from './Account';


const ActionCreators = Object.assign({},
    AuthActions, AccountActions
);

export default ActionCreators;