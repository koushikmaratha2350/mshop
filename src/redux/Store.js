import { createStore, applyMiddleware, compose } from 'redux';
import reducers from './reducers';
import thunkMiddleware from 'redux-thunk';

const middleWare = [thunkMiddleware];

export const Store = compose(applyMiddleware(...middleWare)(createStore)(reducers));
