import * as  ActionTypes from '../actions/ActionTypes';

export default function base(state, action) {
    switch (action.type) {
        case ActionTypes.POST_SIGNIN_PENDING:
        case ActionTypes.POST_SIGNUP_PENDING:
            return {
                ...state,
                type: action.type,
                message: '',
                isRequesting: true,
                userData: {},
            }
            break;
        case ActionTypes.POST_SIGNIN_SUCCESS:
        case ActionTypes.POST_SIGNUP_SUCCESS:
            return {
                ...state,
                type: action.type,
                message: '',
                isRequesting: false,
                userData: action.payload,
            };
            break;
        case ActionTypes.POST_SIGNIN_FAILURE:
        case ActionTypes.POST_SIGNUP_FAILURE:
        case ActionTypes.SIGNOUT:
            return {
                ...state,
                type: action.type,
                message: action.message,
                isRequesting: false,
                userData: {},
            };
            break;
        default:
            return { ...state };
            break;
    }
}