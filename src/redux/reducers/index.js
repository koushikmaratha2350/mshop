import { combineReducers } from 'redux';
import AuthReducer from './Auth';
import AccountReducer from './Account';

const reducers = combineReducers({
    AuthReducer, AccountReducer
});

export default reducers;