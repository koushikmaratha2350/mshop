import * as  ActionTypes from '../actions/ActionTypes';

export default function base(state, action) {
    switch (action.type) {
        case ActionTypes.GET_DASHBOARD_PENDING:
            return {
                ...state,
                type: action.type,
                message: '',
                isRequesting: true,
                dashboardData: {},
            }
            break;
        case ActionTypes.GET_DASHBOARD_SUCCESS:
            return {
                ...state,
                type: action.type,
                message: '',
                isRequesting: false,
                dashboardData: action.payload,
            };
            break;
        case ActionTypes.GET_DASHBOARD_FAILURE:
            return {
                ...state,
                type: action.type,
                message: action.message,
                isRequesting: false,
                dashboardData: {},
            };
            break;
        default:
            return { ...state };
            break;
    }
}