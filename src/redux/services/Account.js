import * as ApiConstants from '../../constants/Api';

export const getDashboard = () => {
  return new Promise((resolve, reject) => {
    fetch(`${ApiConstants.BASE_URL}/devdoctorInfo/getDashboard/2100602140296199`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      }
    })
      .then(response => {
        if (response.status === 200) {
          resolve(response.json());
        } else {
          reject(response.json());
        }
      })
      .catch(err => reject(err.json()));
  });
};
