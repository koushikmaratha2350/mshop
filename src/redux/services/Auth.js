import * as ApiConstants from '../../constants/Api';

export const Login = (reqObj) => {
  //  let {userName, password} = reqObj;  //considering reqObj is holding username and password
  return new Promise((resolve, reject) => {
    fetch(`${ApiConstants.BASE_URL}/devdoctor/login`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(reqObj),
    })
        .then((response) => {
            if (response.status === 200) {
                resolve(response.json());
            } else {
                reject(response.json());
            }
        })
        .catch(err => reject(err.json()));
  });
};

export const Register = (reqObj) => {
    //  let {userName, email, name, userType, mobile, password} = reqObj;  //considering reqObj is holding the data
  return new Promise((resolve, reject) => {
    fetch(`${ApiConstants.BASE_URL}/devdoctor/save`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(reqObj),
    })
        .then((response) => {
            if (response.status === 200) {
                resolve(response.json());
            } else {
                reject(response.json());
            }
        })
        .catch(err => reject(err.json()));
  });
};
