import * as React from 'react';
import {Text, View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import Login from './screens/Login';
import Register from './screens/Register';
import Dashboard from './screens/Dashboard';
import AppLoader from './screens/AppLoader';
import Otp from './screens/Otp';

//hooks instantiated
const Stack = createNativeStackNavigator();

const AuthStack = () => {
  return (
    <Stack.Navigator initialRouteName="Login">
      <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
      <Stack.Screen name="Register" component={Register} options={{headerShown:false}}/>
      <Stack.Screen name="Otp" component={Otp} options={{headerShown:false}}/>
    </Stack.Navigator>
  );
};

const ProtectedStack = () => {
  return (
    <Stack.Navigator headerMode={'none'} initialRouteName="Dashboard">
      <Stack.Screen name="Dashboard" component={Dashboard} options={{headerShown:false}}/>
    </Stack.Navigator>
  );
};

export default function Root() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode={''}  initialRouteName="AuthStack">
        <Stack.Screen name="AppLoader" component={AppLoader} options={{headerShown:false}}/>
        <Stack.Screen name="ProtectedStack" component={ProtectedStack} options={{headerShown:false}}/>
        <Stack.Screen name="AuthStack" component={AuthStack} options={{headerShown:false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}
