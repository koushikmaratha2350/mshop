import AsyncStorage from '@react-native-async-storage/async-storage';


export const getUserSavedData = async () => {
    let userData = await AsyncStorage.getItem('@userData');
    if (userData) {
        return JSON.parse(userData);
    }
    return null;
}

export const saveUserData = async (data) => {
    try {
        return await AsyncStorage.setItem('@userData', JSON.stringify(data));
    } catch (err) {
        return err;
    }
}
export const removeUserData = async () => {
    try {
        return await AsyncStorage.removeItem('@userData');
    } catch (err) {
        return err;
    }
}