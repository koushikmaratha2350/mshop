import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import ActionCreators from '../redux/actions';
import {CommonActions} from '@react-navigation/native';
import * as Storage from '../utils/Storage';

const Login = props => {
  const dispatch = useDispatch();
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  useEffect(() => {
    Storage.removeUserData();   // removed userdata when the user only first renders
  }, []);
  const onSubmit = () => {
    userName && password
      ? dispatch(ActionCreators.Login(userName, password))
      : Alert.alert('Enter UserName and Password');
  };
  const StateType = useSelector(state => state?.AuthReducer.type);
  if (StateType === 'POST_SIGNIN_SUCCESS') {
    props.navigation.dispatch(
      CommonActions.reset({
        index: 1,
        routes: [{name: 'ProtectedStack'}],
      }),
    );
  }

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
      }}>
      <Text style={{fontSize: 25}}>Login</Text>
      <View style={{marginVertical: 10}}>
        <Text style={{justifyContent: 'flex-start', opacity: 0.2}}>
          UserName
        </Text>
        <TextInput
          defaultValue={userName}
          clearButtonMode={'while-editing'}
          onChangeText={txt => setUserName(txt)}
          style={{
            borderWidth: 1,
            borderColor: 'white',
            borderBottomColor: '#CCC',
            minWidth: '90%',
            height: 50,
            fontSize: 16,
          }}
        />
      </View>
      <View style={{marginVertical: 10}}>
        <Text style={{justifyContent: 'flex-start', opacity: 0.2}}>
          Password
        </Text>
        <TextInput
          defaultValue={password}
          clearButtonMode={'while-editing'}
          onChangeText={txt => setPassword(txt)}
          secureTextEntry
          style={{
            borderWidth: 1,
            borderColor: 'white',
            borderBottomColor: '#CCC',
            minWidth: '90%',
            height: 50,
            fontSize: 16,
          }}
        />
      </View>
      <TouchableOpacity
        onPress={() => onSubmit()}
        style={{
          margin: 50,
          backgroundColor: '#CC2323',
          paddingHorizontal: 45,
          paddingVertical: 15,
          borderRadius: 20,
        }}>
        <Text style={{fontSize: 20, color: '#FFF'}}>Login</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.navigation.navigate('Register')}>
        <Text style={{fontWeight: '600', color: 'blue'}}>
          No Account? Register
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({});
