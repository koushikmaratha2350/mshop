import React, { useEffect } from 'react'
import { Text, StyleSheet, View, ActivityIndicator } from 'react-native'
import {getUserSavedData} from '../utils/Storage';
import _ from 'lodash';
import {CommonActions} from '@react-navigation/native';

const AppLoader = ({props,navigation}) => {

  useEffect(() => {
    getUserSavedData()
      .then(userData => {
        if (_.get(userData, 'email')) {
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                { name: "ProtectedStack" },
              ],
            })
          )
        } else {
          navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [
                { name: "ProtectedStack" },
              ],
            })
          )
        }
      })
      .catch(err => {
        console.log(err)
      })
  }, [])
  return (
    <View style={styles.container}>
      <ActivityIndicator size={'large'} color='#6AD2D7' />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1, alignItems: 'center', justifyContent: 'center'
  }
})

export default AppLoader;
