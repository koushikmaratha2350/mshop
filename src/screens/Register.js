import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import ActionCreators from '../redux/actions';

const Register = props => {
  const dispatch = useDispatch();
  const [name, setName] = useState('');
  const [mobile, setMobile] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const onSubmit = () => {
    name && mobile && email && password
      ? dispatch(ActionCreators.Register(email, name, mobile, password))
      : Alert.alert('Enter Missing Details');
  };
  const AuthState = useSelector(state => state.AuthReducer);
  if (AuthState.type === 'POST_SIGNUP_SUCCESS') {
    props.navigation.navigate('Otp');
  }
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
      }}>
      <Text style={{fontSize: 25}}>Register</Text>
      <View style={{marginVertical: 10}}>
        <Text style={{justifyContent: 'flex-start', opacity: 0.2}}>Name</Text>
        <TextInput
          onChangeText={txt => setName(txt)}
          defaultValue={name}
          clearButtonMode={'while-editing'}
          style={{
            borderWidth: 1,
            borderColor: 'white',
            borderBottomColor: '#CCC',
            minWidth: '90%',
            height: 50,
            fontSize: 16,
          }}
        />
      </View>
      <View style={{marginVertical: 10}}>
        <Text style={{justifyContent: 'flex-start', opacity: 0.2}}>Email</Text>
        <TextInput
          onChangeText={txt => setEmail(txt)}
          defaultValue={email}
          clearButtonMode={'while-editing'}
          style={{
            borderWidth: 1,
            borderColor: 'white',
            borderBottomColor: '#CCC',
            minWidth: '90%',
            height: 50,
            fontSize: 16,
          }}
        />
      </View>
      <View style={{marginVertical: 10}}>
        <Text style={{justifyContent: 'flex-start', opacity: 0.2}}>Mobile</Text>
        <TextInput
          onChangeText={txt => setMobile(txt)}
          defaultValue={mobile}
          clearButtonMode={'while-editing'}
          style={{
            borderWidth: 1,
            borderColor: 'white',
            borderBottomColor: '#CCC',
            minWidth: '90%',
            height: 50,
            fontSize: 16,
          }}
        />
      </View>
      <View style={{marginVertical: 10}}>
        <Text style={{justifyContent: 'flex-start', opacity: 0.2}}>
          Password
        </Text>
        <TextInput
          onChangeText={txt => setPassword(txt)}
          defaultValue={password}
          clearButtonMode={'while-editing'}
          secureTextEntry
          style={{
            borderWidth: 1,
            borderColor: 'white',
            borderBottomColor: '#CCC',
            minWidth: '90%',
            height: 50,
            fontSize: 16,
          }}
        />
      </View>
      <TouchableOpacity
        onPress={() => onSubmit()}
        style={{
          margin: 50,
          backgroundColor: '#CC2323',
          paddingHorizontal: 45,
          paddingVertical: 15,
          borderRadius: 20,
        }}>
        <Text style={{fontSize: 20, color: '#FFF'}}>Register</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
        <Text style={{fontWeight: '600', color: 'blue'}}>
          Have Account? Login
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Register;

const styles = StyleSheet.create({});
