import React,{useEffect} from 'react'
import { StyleSheet, Text, View } from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import ActionCreators from '../redux/actions';

const Dashboard = (props) => {
    const dispatch = useDispatch();
    const DashState = useSelector(state => state?.AccountReducer);
    useEffect(() => {
        dispatch(ActionCreators.GetDashboardData());
    }, []);
    const dashBoardData = DashState.dashboardData;
    return (
        <View>
            <Text>{JSON.stringify(dashBoardData)}</Text>
        </View>
    )
}

export default Dashboard;

const styles = StyleSheet.create({})
