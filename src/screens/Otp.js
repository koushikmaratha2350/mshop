import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {useSelector} from 'react-redux';
import {CommonActions} from '@react-navigation/native';


const Otp = props => {
  const [otpValue, setOTPValue] = useState('');
  const AuthState = useSelector(state => state.AuthReducer);
  const onSubmit = () => {
    otpValue && AuthState?.userData?.otpInfo?.otp === otpValue
      ? props.navigation.dispatch(
          CommonActions.reset({
            index: 1,
            routes: [{name: 'ProtectedStack'}],
          }),
        )
      : Alert.alert('Enter OTP Value');
  };
  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#FFF',
      }}>
      <Text style={{fontSize: 25}}>OTP Verification</Text>
      <View style={{marginVertical: 10}}>
        <Text style={{justifyContent: 'flex-start', opacity: 0.2}}>OTP</Text>
        <TextInput
          defaultValue={otpValue}
          clearButtonMode={'while-editing'}
          onChangeText={txt => setOTPValue(txt)}
          style={{
            borderWidth: 1,
            borderColor: 'white',
            borderBottomColor: '#CCC',
            minWidth: '90%',
            height: 50,
            fontSize: 16,
          }}
        />
      </View>
      <TouchableOpacity
        onPress={() => onSubmit()}
        style={{
          margin: 50,
          backgroundColor: '#CC2323',
          paddingHorizontal: 45,
          paddingVertical: 15,
          borderRadius: 20,
        }}>
        <Text style={{fontSize: 20, color: '#FFF'}}>Validate</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => props.navigation.navigate('Register')}>
        <Text style={{fontWeight: '600', color: 'blue'}}>
          No Account? Register
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Otp;

const styles = StyleSheet.create({});
